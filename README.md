Phased Array Simulation
=======================

This is an academic simulation of **Phased Arrays** using **Python Scientific
Stack**. [Click here](#phased-arrays-introduction) to know more about Phased
Arrays. The simulation is done using phasor superposition method.

![Simulation output](static/array.mp4)  
Here is the [simulation output on Vimeo](https://vimeo.com/236274273) showing
the result of a single linear array whose beam is being slewed by controlling
phase of the array elements.

## Contents
1. [Prerequisites](#prerequisites)
2. Installation
3. Usage
4. [Phased Arrays: Introduction](#phased-arrays-introduction)
5. Simulation details
6. [License](#license)

## Prerequisites
1. GNU/Linux OS
2. Python 3
3. Numpy
4. Matplotlib
5. ImageMagik
6. FFMpeg

GNU/Linux is required to run a bash shell script. This may be removed in the
future. Requirements 2, 3 and 4 can be met by using
[Anaconda Distribution](https://www.anaconda.com/distribution/). This is
recommended. ImageMagik is for cropping each frame. FFMpeg is used for creating
video from simulation frames.

## Phased Arrays: Introduction
[Phased array](https://en.wikipedia.org/wiki/Phased_array) is a technology used
in radars, sonars and some RF communication systems. A regular radar antenna
has to be moved around (slewed) mechanically to point its beam in any required
direction. Phased array instead has a large antenna made of several tiny
antennae called TR (transmit/receive) elements, to achieve much more at higher
speeds.

The phase and amplitude of RF emission from each TR module can be controlled
independantly. The RF emissions from the individual TR elements combine to form
the overall beam of the array. By cleverly controlling the phase and amplitude
of each TR element, the following can be achieved without any mechanical
movement:  
- The direction of the beam can be changed (very fast compared to mechanical
slewing)
- The main beam can be made as narrow or as wide as required
- The beam side lobes can be reduced

![Representative diagram](https://upload.wikimedia.org/wikipedia/commons/4/4a/Phased_array_animation_with_arrow_10frames_371x400px_100ms.gif)

Since mechanical slewing is not needed, phased array radars are capable of
searching and tracking a large number of targets simultaneously, unlike regular
radars. Phased arrays are also being tested for applications in mobile
communications. The mobile tower beam will be able to point directly at you,
improving coverage, datarate and power requirements.

## License
    A Simulation of Phased Arrays
    Copyright (C) 2017 Gokul Das B

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
