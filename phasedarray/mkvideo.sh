#!/usr/bin/env sh

# Simulation of Phased Arrays
# Copyright (C) 2017 Gokul Das B

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation. You should have received a copy of the
# GNU General Public License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.


python array.py

mogrify -gravity east -crop 1280x720+0+0 img/frame*.png

# Convert frames to video: 
# http://hamelot.io/visualization/using-ffmpeg-to-convert-a-set-of-images-into-a-video/
ffmpeg -r 25 -f image2 -s 1280x720 -i img/frame%03d.png -vcodec libx264 -crf 15  -pix_fmt yuv420p array.mp4
