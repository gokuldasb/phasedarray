# Simulation of Phased Arrays
# Copyright (C) 2017 Gokul Das B

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation. You should have received a copy of the
# GNU General Public License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams['savefig.bbox'] = 'tight'
matplotlib.rcParams['savefig.pad_inches'] = 0.0


class Radiator:
    def __init__(self, magnitude, phase, pos_x, pos_y):
        self.m = magnitude
        self.p = phase
        self.x = pos_x
        self.y = pos_y

    def field(self, X, Y, wavelength):
        d = np.sqrt((X - self.x) ** 2 + (Y - self.y) ** 2)
        mag = self.m / (d ** 2)
        phi = self.p - (2 * np.pi * d / wavelength)
        return mag, phi


class RadiatorArray:
    wavelength = 2.0
    u = np.linspace(-320, 320, 1280)
    v = np.linspace(-180, 180, 720)
    X, Y = np.meshgrid(u, v)

    def __init__(self, radiators):
        self.radiators = radiators

    def calculate_field(self):
        fx = np.zeros(self.X.shape)
        fy = np.zeros(self.Y.shape)
        for radiator in self.radiators:
            m, p = radiator.field(self.X, self.Y, self.wavelength)
            fx += m * np.cos(p)
            fy += m * np.sin(p)
        mag = np.sqrt(fx ** 2 + fy ** 2)
        phi = np.arctan2(fy, fx)
        return mag, phi

    @staticmethod
    def clip(field, cliplevel=1.0):
        a = np.sort(field.flatten())
        n = int(a.size * cliplevel / 100)
        maxm = a[-n]
        mask = field > maxm
        return np.ma.masked_array(field, mask=mask)

    def plot(self, contours=False, cliplevel=1.0):
        field, _ = self.calculate_field()
        mfield = self.clip(field, cliplevel)

        fig = plt.figure(figsize=(16, 9), dpi=100)
        ax = fig.add_subplot(111)
        ax.axis('off')

        bglevels = np.linspace(mfield.min(), mfield.max(), 100)
        ax.contourf(self.X, self.Y, mfield, cmap='viridis', levels=bglevels)

        if contours:
            llevels = np.linspace(mfield.min(), mfield.max(), 20)
            cs = ax.contour(self.X, self.Y, mfield,
                            cmap='Reds', levels=llevels)
            ax.clabel(cs, inline=1, fontsize=10)

        for i in self.radiators:
            ax.scatter(i.x, i.y)

        return fig


class LinearArray(RadiatorArray):

    @staticmethod
    def arrayindex(n):
        lim = (n - 1) / 2
        return np.arange(-lim, lim + 1)

    def spacing(self, phase_spacing):
        return self.wavelength * phase_spacing / 360

    @staticmethod
    def element_phase_increment(boresight, phase_spacing):  # in degrees
        bsr = np.deg2rad(boresight)
        return phase_spacing * (-np.pi / 180) * np.cos(bsr)

    def __init__(self, boresight, n=11, phase_spacing=180):
        indices = self.arrayindex(n)
        del_phi = self.element_phase_increment(boresight, phase_spacing)
        spacing = self.spacing(phase_spacing)
        rads = [Radiator(1.0, del_phi * i, spacing * i, 0.0) for i in indices]
        super().__init__(rads)


frames = 500
for i in range(frames):
    angle = 180 * i / frames
    linear = LinearArray(angle)
    fig = linear.plot()
    label = "{:3d}$\degree$".format(int(angle))
    fig.gca().text(150, -150, label, family='monospace', size=20)
    print("Saving image {:3d} of {:3d}".format(i, frames))
    fig.savefig("img/frame{:03d}.png".format(i))
    plt.close()
